/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.test;

import com.tudorhanganu.business.SendAndReceive;
import com.tudorhanganu.data.AdvancedEmail;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import javax.activation.DataSource;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.MailException;
import jodd.mail.ReceivedEmail;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Tudor Hanganu
 */
public class SendAndReceiveTest {
    
    private final Logger log = LoggerFactory.getLogger(getClass().getName());
    
    SendAndReceive sendAndReceive = new SendAndReceive();
    
    String[] receiver = {"tudorreceivemail@gmail.com"};
    
    // TEST METHODS FOR SENDING EMAILS
    
    /**
     * Test of sendEmail method, of class SendAndReceive.
     * Checking if the email can be sent and received, basic email
     */
    @Test
    public void testSendEmail() {
        
        log.info("START testSendEmail()");
        
        AdvancedEmail sendMail = new AdvancedEmail("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        AdvancedEmail receiveMail = new AdvancedEmail("imap.gmail.com", "tudorreceivemail@gmail.com", "nanapapa123");
        
        Email emailToCompare = sendAndReceive.sendEmail(sendMail, "Test Mail 123", "Test Mail Subject", "", receiver , new String[0], new String[0], new String[0], new String[0]);
        
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            log.error("Threaded sleep failed", e);
        }
        
        ReceivedEmail[] emails = sendAndReceive.receiveEmail(receiveMail);
        //log.info("right here");
        //log.info(emails[0].messages().get(0).getContent().toString());

        boolean compare = false;
        
        if ((emailToCompare.to()[0].toString()).equals((emails[0].to()[0]).toString())){
            if ((emailToCompare.subject().toString()).equals(emails[0].subject().toString())){
                if ((emailToCompare.from().toString()).equals(emails[0].from().toString())){
                    compare = true;
                
                }
            }
        }
        
        assertTrue("Error, not same value", compare);
    }
    
    /**
     * Test of send mail method, sending 3 emails, verifying that the recipient did in fact receive 3 emails and not only one.
     */
    @Test
    public void testSendMultipleEmails(){
        
        log.info("START testSendMultipleEmails()");
        
        AdvancedEmail sender = new AdvancedEmail("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        AdvancedEmail receive = new AdvancedEmail("imap.gmail.com", "tudorreceivemail@gmail.com", "nanapapa123");
        
        //sending 3 emails and checking if the receiving email got all 3
        Email toCompare = sendAndReceive.sendEmail(sender, "Email 1", "testSendMultipleEmails", "", receiver, new String[0], new String[0], new String[0], new String[0]);
        Email toCompare2 = sendAndReceive.sendEmail(sender, "Email 2", "testSendMultipleEmails2", "", receiver, new String[0], new String[0], new String[0], new String[0]);
        Email toCompare3 = sendAndReceive.sendEmail(sender, "Email 3", "testSendMultipleEmails3", "", receiver, new String[0], new String[0], new String[0], new String[0]);
        
        //changed the timer period, since we are sending 3 mails.
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            log.error("Threaded sleep failed", e);
        }
        
        ReceivedEmail[] emails = sendAndReceive.receiveEmail(receive);
        
        boolean count = false;
        
        if (emails.length == 3){
            if (toCompare.subject().toString().equals("testSendMultipleEmails") && 
                toCompare2.subject().toString().equals("testSendMultipleEmails2") && 
                toCompare3.subject().toString().equals("testSendMultipleEmails3")){
                count = true;
            }
        }
        
        assertTrue("Did not receive 3 emails", count);
    }
    
    /**
     * Test of send mail method with cc, verifying that the CC address does in fact receive the email
     */
    @Test
    public void testSendEmailWithCc(){
        
        log.info("START testSendEmailWithCc()");
        
        AdvancedEmail sender = new AdvancedEmail("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        AdvancedEmail receive = new AdvancedEmail("imap.gmail.com", "tudorcc0@gmail.com", "nanapapa123");
        
        String[] receivers = {"tudorcc0@gmail.com"};
        
        Email toCompare = sendAndReceive.sendEmail(sender, "This is CC function", "testSendEmailWithCC", "", receivers, new String[0], new String[0], new String[0], new String[0]);
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            log.error("Threaded sleep failed", e);
        }
        
        ReceivedEmail[] emailsCC1 = sendAndReceive.receiveEmail(receive);
        log.info(emailsCC1[0].from().toString());
        
        boolean checkIfReceived = false;
        
        if (emailsCC1.length > 0){
            if ((emailsCC1[0].subject().toString()).equals(toCompare.subject().toString())){
                checkIfReceived = true;
            }
        }
        
        assertTrue("Not all emails were received", checkIfReceived);
        
    }
    
    /**
     * Test of send mail method with null value as a parameter
     */
    @Test (expected = NullPointerException.class)
    public void testSendNullInfoTest() {
        AdvancedEmail sendMail = new AdvancedEmail("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        
        // setting variables to visualize the error
        String[] receivers = {"tudorreceivemail@gmail.com"};
        String content = "Testing Null Values";
        String subject = "Null Pointer";
        String HTML = "";
        String[] cc = null;
        String[] bcc = new String[1];
        String[] attachments = new String[1];
        String[] embAttachments = new String[1];
        
        Email email = sendAndReceive.sendEmail(sendMail, content, subject, HTML, receivers, cc, bcc, attachments, embAttachments);
        
    }
    
    /**
     * Test of send mail method with an invalid value as the host name
     */
    @Test (expected = ClassFormatError.class)
    public void testSendWrontHost() {
        AdvancedEmail decoyMail = new AdvancedEmail("smtp", "tudorsendmail@gmail.com", "nanapapa123");
        
        String[] receiver = {"tudorreceivemail@gmail.com"};
        
        Email email = sendAndReceive.sendEmail(decoyMail, "Sending mail with invalid host name", "sendWrongHostName", "", receiver, new String[0], new String[0], new String[0], new String[0]);
    }
    
    
    //**********************************
    // TEST METHODS FOR RECEIVING EMAILS
    
    /**
     * Test of receiveEmail method, of class SendAndReceive.
     */
    @Test
    public void testReceiveHTML() {
        AdvancedEmail sender = new AdvancedEmail("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        AdvancedEmail receive = new AdvancedEmail("imap.gmail.com", "tudorreceivemail@gmail.com", "nanapapa123");
        
        String[] receiver = {"tudorreceivemail@gmail.com"};
        
        Email toCompare = sendAndReceive.sendEmail(sender, "This is an email with embedded HTML", "testReceiveHTML", "<img alt=\"Qries\" src=\"https://www.qries.com/images/banner_logo.png\"\n" +
"         width=150\" height=\"70\">", receiver, new String[0], new String[0], new String[0], new String[0]);
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            log.error("Threaded sleep failed", e);
        }
        
        ReceivedEmail[] emails = sendAndReceive.receiveEmail(receive);
        
        boolean isCorrect = false;
        
        if (emails.length != 0){
            if ((toCompare.subject().toString()).equals(emails[0].subject().toString())){
                isCorrect = true;
            }
        }
        
        assertTrue("Did not pass through, invalid input", isCorrect);
        
    }
    
    /**
     * Test of receiveEmail method, sending a BCC, checking if it works
     */
    @Test
    public void testReceiveWithBCC(){
        AdvancedEmail sender = new AdvancedEmail("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        AdvancedEmail receive = new AdvancedEmail("imap.gmail.com", "tudorcc0@gmail.com", "nanapapa123");
        
        String[] receiver = {"tudorreceivemaill@gmail.com"};
        String[] bcc = {"tudorcc0@gmail.com"};
        
        Email initial = sendAndReceive.sendEmail(sender, "Testing with bcc", "testReceiveWithBCC", "", receiver, new String[0], bcc, new String[0], new String[0]);
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            log.error("Threaded sleep failed", e);
        }
        
        ReceivedEmail[] emails = sendAndReceive.receiveEmail(receive);
        
        boolean isCorrect = false;
        
        if (emails.length > 0){
            if ((initial.subject().toString()).equals(emails[0].subject().toString())){
                isCorrect = true;
            }
        }
        
        assertTrue("User did not receive BCC of the email", isCorrect);
    }
    
    /**
     * Test of receiveEmail method, with an attachment that cannot be stored, so it throws an exception
     */
    @Test (expected = MailException.class)
    public void testReceiveWithInvalidAttachments(){
        AdvancedEmail sender = new AdvancedEmail("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        AdvancedEmail receive = new AdvancedEmail("imap.gmail.com", "tudorreceivemail@gmail.com", "nanapapa123");
        
        String[] receiver = {"tudorreceivemail@gmail.com"};
        String[] attachmentImage = {"dolphin.jpg"};
        
        Email email = sendAndReceive.sendEmail(sender, "This is an email with an attachment", "testReceiveWithAttachment", "", receiver, new String[0], new String[0], attachmentImage, new String[0]);
        
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            log.error("Threaded sleep failed", e);
        }
        
        ReceivedEmail[] emails = sendAndReceive.receiveEmail(receive);
        
        // code to process attachment
        List<EmailAttachment<? extends DataSource>> attachments = emails[0].attachments();
                        if (attachments != null) {
                            log.info("+++++");
                            attachments.stream().map((attachment) -> {
                                log.info("name: " + attachment.getName());
                                return attachment;
                            }).map((attachment) -> {
                                log.info("cid: " + attachment.getContentId());
                                return attachment;
                            }).map((attachment) -> {
                                log.info("size: " + attachment.getSize());
                                return attachment;
                            }).forEachOrdered((attachment) -> {
                                attachment.writeToFile(
                                        new File("c:\\temp", attachment.getName()));
                            });
                        }
        
    }
    
    /**
     * Test of receiveEmail method, expecting email to not receive anything if not entered properly.
     */
    @Test
    public void testReceiveWrongEmail() {
        AdvancedEmail sender = new AdvancedEmail("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        AdvancedEmail receive = new AdvancedEmail("imap.gmail.com", "tudorreceivemail@gmail.com", "nanapapa123");
        
        String[] receiver = {"budorreceivemail@gmail.com"};
        
        Email email = sendAndReceive.sendEmail(sender, "Test email with wront email address", "testReceiveWrongEmail", "", receiver, new String[0], new String[0], new String[0], new String[0]);
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            log.error("Threaded sleep failed", e);
        }
        
        ReceivedEmail[] emails = sendAndReceive.receiveEmail(receive);
        
        boolean didReceive = true;
        
        if (emails.length == 0){
            didReceive = false;
        }
        
        assertFalse("Email did not go through", didReceive);
        
        
    }
    
    /**
     * Test of receiveEmail method, expecting null pointer since credentials are set to null.
     */
    @Test (expected = NullPointerException.class)
    public void testReceiveNullCredentials() {
        AdvancedEmail sender = new AdvancedEmail("smtp.gmail.com", "tudorsendmail@gmail.com", "nanapapa123");
        AdvancedEmail receive = new AdvancedEmail("imap.gmail.com", null, null);
        
        String[] receiver = {"tudorreceivemail@gmail.com"};
        
        sendAndReceive.sendEmail(sender, "Testing with null credentials", "TestReceiveNullCredentials", "", receiver, new String[0], new String[0], new String[0], new String[0]);
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            log.error("Threaded sleep failed", e);
        }
        
        // will create a null pointer exception since credentials are null.
        ReceivedEmail[] emails = sendAndReceive.receiveEmail(receive);
    }
    
}
