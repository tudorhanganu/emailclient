/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.config;

import com.tudorhanganu.business.sqlOperations;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.slf4j.LoggerFactory;

/**
 * @author Tudor Hanganu
 */
public class ConfigBean {
    
    private StringProperty username;
    private String host;
    private StringProperty emailAddress;
    private StringProperty password;
    private StringProperty imapURL;
    private StringProperty smtpURL;
    private StringProperty imapPort;
    private StringProperty smtpPort;
    private StringProperty sqlURL;
    private StringProperty dbName;
    private StringProperty dbPort;
    private StringProperty dbUsername;
    private StringProperty dbPassword;
    
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(ConfigBean.class);
    
    public ConfigBean(){
        this( "", "", "", "", "", "", "", "","","","","","");
    }
    
    /**
     * Simple email send/receive bean
     * 
     * @param host necessary to send
     * @param emailAddress email address to use to send email from.
     * @param password password of the email address 
     */
    public ConfigBean(String host, String emailAddress, String password){
        this.smtpURL = new SimpleStringProperty(host);
        this.emailAddress = new SimpleStringProperty(emailAddress);
        this.password = new SimpleStringProperty(password);
        this.imapURL = new SimpleStringProperty(host);
    }
    
    /**
     * Constructor to use for SQL connection
     * 
     * @param sqlUrl the url of the SQL server
     * @param dbname database name
     * @param dbusername login username
     * @param dbpassword database password to access the server
     */
    public ConfigBean(String sqlUrl, String dbname, String dbusername, String dbpassword){
        this.dbPort = new SimpleStringProperty("3306");
        
        String url = ("jdbc:mysql://" + sqlUrl + ":" + this.sqlPort() + "/" + dbname);
        
        this.sqlURL = new SimpleStringProperty(url);
        this.dbName = new SimpleStringProperty(dbname);
        this.dbUsername = new SimpleStringProperty(dbusername);
        this.dbPassword = new SimpleStringProperty(dbpassword);
        
    }
    
    /**
     * This is the full constructor, holding all the parameters.
     * 
     * @param username
     * @param host
     * @param emailAddress
     * @param password
     * @param sqlURL
     * @param dbName
     * @param dbUsername
     * @param dbPassword
     */
    public ConfigBean(String username, String host, String emailAddress, String password, String imapPort, String imapURL, String smtpPort, String smtpURL, String sqlURL, String dbName,String dbPort, String dbUsername, String dbPassword) {
        this.username = new SimpleStringProperty(username);
        this.host = host;
        this.emailAddress = new SimpleStringProperty(emailAddress);
        this.password = new SimpleStringProperty(password);
        this.imapPort = new SimpleStringProperty(imapPort);
        this.imapURL = new SimpleStringProperty(imapURL);
        this.smtpPort = new SimpleStringProperty(smtpPort);
        this.smtpURL = new SimpleStringProperty(smtpURL);
        this.sqlURL = new SimpleStringProperty(sqlURL);
        this.dbName = new SimpleStringProperty(dbName);
        this.dbPort = new SimpleStringProperty(dbPort);
        this.dbUsername = new SimpleStringProperty(dbUsername);
        this.dbPassword = new SimpleStringProperty(dbPassword);
    }

    public StringProperty getEmailAddress() {
        return this.emailAddress;
    }
    
    public void setEmailAddress(String emailAddress) {
        this.emailAddress.set(emailAddress);
    }
    
    public StringProperty getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username.set(username);
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public StringProperty getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public StringProperty getSqlURL() {
        return this.sqlURL;
    }

    public void setSqlURL(String sqlURL) {
        this.sqlURL.set(sqlURL);
    }

    public StringProperty getDbName() {
        return this.dbName;
    }

    public void setDbName(String dbName) {
        this.dbName.set(dbName);
    }

    public StringProperty getDbUsername() {
        return this.dbUsername;
    }

    public void setDbUsername(String dbUsername) {
        this.dbUsername.set(dbUsername);
    }

    public StringProperty getDbPassword() {
        return this.dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword.set(dbPassword);
    }
    
    public StringProperty getImapURL() {
        return this.imapURL;
    }

    public void setImapURL(String imapURL) {
        this.imapURL.set(imapURL);
    }
    
    public StringProperty getImapPort() {
        return this.imapPort;
    }

    public void setImapPort(String imapPort) {
        this.imapPort.set(imapPort);
    }
    
    public StringProperty getSmtpURL() {
        return this.smtpURL;
    }

    public void setSmtpURL(String smtpURL) {
        this.smtpURL.set(smtpURL);
    }
    
    public StringProperty getSmtpPort() {
        return this.smtpPort;
    }
    
    public void setSmtpPort(String smtpPort) {
        this.smtpPort.set(smtpPort);
    }

    public StringProperty getDbPort() {
        return this.dbPort;
    }

    public void setDbPort(String dbPort) {
        this.dbPort.set(dbPort);
    }
    
    public String username(){
        return this.username.get();
    }
    
    public String password(){
        return this.password.get();
    }
    
    public String email(){
        return this.emailAddress.get();
    }
    
    public String dbname(){
        return this.dbName.get();
    }
    
    public String dbuser(){
        return this.dbUsername.get();
    }
    
    public String dbpass(){
        return this.dbPassword.get();
    }
    
    public String sqlurl(){
        return this.sqlURL.get();
    }
    
    public String smtpPort(){
        return this.smtpPort.get();
    }
    
    public String smtpUrl(){
        return this.smtpURL.get();
    }
    
    public String imapPort(){
        return this.imapPort.get();
    }
    
    public String imapUrl(){
        return this.imapURL.get();
    }
    
    public String sqlPort(){
        return this.dbPort.get();
    }
   
}
