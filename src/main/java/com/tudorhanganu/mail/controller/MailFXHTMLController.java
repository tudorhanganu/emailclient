package com.tudorhanganu.mail.controller;

/**
 *
 * @author Tudor Hanganu
 */

import com.tudorhanganu.application.MainApp;
import com.tudorhanganu.business.sqlOperations;
import com.tudorhanganu.business.SendAndReceive;
import com.tudorhanganu.config.ConfigBean;
import com.tudorhanganu.data.AdvancedEmail;
import com.tudorhanganu.exceptions.InvalidEmailException;
import com.tudorhanganu.properties.PropertiesManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Base64;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.HTMLEditor;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import jodd.mail.ReceivedEmail;

/**
 *
 * @author Tudor Hanganu
 */
public class MailFXHTMLController {

    ConfigBean config = new ConfigBean();
    
    sqlOperations crud;
    
    SendAndReceive sAndR = new SendAndReceive();
    
    PropertiesManager propMan = new PropertiesManager();
    
    List<File> fileToAdd = new ArrayList<File>();
    
    AdvancedEmail mailDisplaying;
    
    @FXML 
    private AnchorPane mailFXHTMLLayout; 

    @FXML 
    private TextField mailFXHTMLToField; 

    @FXML 
    private TextField mailFXHTMLCcField;

    @FXML 
    private TextField mailFXHTMLBccField; 

    @FXML 
    private TextField mailFXHTMLSubjectField;

    @FXML 
    private HTMLEditor mailFXHTMLEditor; 
    
    @FXML
    private Button sendBtn;
    
    @FXML
    private Button addAttach;

    @FXML
    private Button saveAttach;
    
    @FXML
    void addAttachment(ActionEvent event) {
        FileChooser fc = new FileChooser();
        fc.setTitle("Load");
        List<File> pick = fc.showOpenMultipleDialog(null);
        
        if (pick != null){
            fileToAdd = pick;
        }
        else {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Add");
            alert.setHeaderText("Add Attachment:");
            alert.setContentText("You did not add an attachment");

            alert.showAndWait();
        }
    }

    @FXML
    void saveAttachment(ActionEvent event) throws FileNotFoundException, IOException {
        
        if (mailDisplaying.Email.attachments().size() != 0){
            DirectoryChooser directoryPicker = new DirectoryChooser();
            directoryPicker.setTitle("Choose Directory");
            File selectedDirectory = directoryPicker.showDialog(MainApp.primaryStage);

            String path = selectedDirectory.getAbsolutePath();
            
            for (EmailAttachment attach : mailDisplaying.Email.attachments()){
                
                File file = new File(path, attach.getName());
                
                FileOutputStream fos = new FileOutputStream(file);
                
                fos.write(attach.toByteArray());
            }
            
        }
        else {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Save");
            alert.setHeaderText("Save Attachment:");
            alert.setContentText("Email does not contain any attachments");

            alert.showAndWait();
        }
        
        
    }

    @FXML
    void sendEmail(ActionEvent event) throws InvalidEmailException, IOException {
        String subject = mailFXHTMLSubjectField.getText();
        String tos;
        String ccs;
        String bccs;
        
        String[] toAddresses;
        String[] ccAddresses;
        String[] bccAddresses;
        
        String to = mailFXHTMLToField.getText();
        
        propMan.load(config, "", "config");
        crud = new sqlOperations(config);
        
        if (!mailFXHTMLToField.getText().isEmpty()){
            tos = mailFXHTMLToField.getText();
            tos = tos.substring(0, tos.length());
            toAddresses = tos.split(",");
        }
        else { toAddresses = new String[0]; }
        
        if (!mailFXHTMLCcField.getText().isEmpty()){
            ccs = mailFXHTMLCcField.getText();
            ccs = ccs.substring(0, ccs.length());
            ccAddresses = ccs.split(",");
        }
        else { ccAddresses = new String[0]; }
        
        
        if (!mailFXHTMLBccField.getText().isEmpty()){
            bccs = mailFXHTMLBccField.getText();
            bccs = bccs.substring(0, bccs.length());
            bccAddresses = bccs.split(",");
        }
        else { bccAddresses = new String[0]; }
        
        ArrayList<File> files = new ArrayList<File>();
        for (File file : fileToAdd){
            files.add(file);
        }
        
        Email email = sAndR.sendEmail(config, toAddresses, subject, mailFXHTMLEditor.getHtmlText(), "", ccAddresses, bccAddresses, files, new ArrayList<>());
        
        AdvancedEmail mail = new AdvancedEmail();
        mail.Email = email;
        LocalDate now = LocalDate.now();
        mail.setReceivedDate(Date.valueOf(now));
        mail.setFolderID(1);
        
        crud.addEmail(mail);
        
    }
    
    void initialize() throws IOException {
        propMan.load(config, "", "config");
        
        crud = new sqlOperations(config);
        
        assert mailFXHTMLLayout != null : "fx:id=\"mailFXHTMLLayout\" was not injected: check your FXML file 'mailFXHTMLLayoutView.fxml'.";
        assert mailFXHTMLToField != null : "fx:id=\"mailFXHTMLToField\" was not injected: check your FXML file 'mailFXHTMLLayoutView.fxml'.";
        assert mailFXHTMLCcField != null : "fx:id=\"mailFXHTMLCcField\" was not injected: check your FXML file 'mailFXHTMLLayoutView.fxml'.";
        assert mailFXHTMLBccField != null : "fx:id=\"mailFXHTMLBccField\" was not injected: check your FXML file 'mailFXHTMLLayoutView.fxml'.";
        assert mailFXHTMLSubjectField != null : "fx:id=\"mailFXHTMLSubjectField\" was not injected: check your FXML file 'mailFXHTMLLayoutView.fxml'.";
        assert mailFXHTMLEditor != null : "fx:id=\"mailFXHTMLEditor\" was not injected: check your FXML file 'mailFXHTMLLayoutView.fxml'.";

    }
    
    /**
     *  Takes a bean and displays the info in the form and text box
     * 
     * @param bean
     * @throws IOException
     */
    public void displayEmailInfo(AdvancedEmail bean) throws IOException{
        mailDisplaying = bean;
        
        this.initialize();
        
        mailFXHTMLEditor.setHtmlText(bean.Email.messages().get(0).getContent());
        
        mailFXHTMLSubjectField.setText(bean.getSubjectField().getValue());
        
        StringBuilder sb = new StringBuilder();
        
        if (crud == null){
            System.out.println("NULL");
        }
        else {
            String[] tos = crud.getToAddresses(bean.getEmailID());
            String[] ccs = crud.getCcAddresses(bean.getEmailID());
            String[] bccs = crud.getBccAddresses(bean.getEmailID());
            
            for (String to : tos){ sb.append(to); sb.append(","); }
            mailFXHTMLToField.setText(sb.toString());
            
            sb = new StringBuilder();
            for (String cc : ccs){ sb.append(cc); sb.append(","); }
            mailFXHTMLCcField.setText(sb.toString());
            
            sb = new StringBuilder();
            for (String bcc : bccs){ sb.append(bcc); sb.append(","); }
            mailFXHTMLBccField.setText(sb.toString());
            
            
            String htmlTextCurrent = mailFXHTMLEditor.getHtmlText();
            mailFXHTMLEditor.setHtmlText(htmlTextCurrent + "<br><br>" + "There are " + bean.Email.attachments().size() + "Attachments in this email");
            htmlTextCurrent = mailFXHTMLEditor.getHtmlText();
            
            if (bean.Email.attachments().size() != 0){
                for (EmailAttachment attach : bean.Email.attachments()){
                    File file = new File(attach.getName());
                    
                    mailFXHTMLEditor.setHtmlText(htmlTextCurrent + 
                            "<img src=\"" + getClass().getResource(attach.getName()) +  "\" width=\"32\" height=\"32\" >" +
                            "<br><br>" + "If you would like to save them press the save attachment button and select a directory");
                }
            }
        }
    }

    /**
     *  Set config bean in the controller
     * 
     * @param config
     */
    public void setConfig(ConfigBean config){
        this.config = config;
    }
   
    /**
     * sets the form to be disabled
     */
    public void setFocusOff(){
        mailFXHTMLToField.setDisable(true);
        mailFXHTMLCcField.setDisable(true);
        mailFXHTMLBccField.setDisable(true);
        mailFXHTMLSubjectField.setDisable(true);
    }
    
    /**
     * sets the form to be enabled
     */
    public void setFocusOn(){
        mailFXHTMLToField.setDisable(false);
        mailFXHTMLCcField.setDisable(false);
        mailFXHTMLBccField.setDisable(false);
        mailFXHTMLSubjectField.setDisable(false);
    }

    public void setAllTextFieldsBlank(){
        mailFXHTMLToField.setText("");
        mailFXHTMLCcField.setText("");
        mailFXHTMLBccField.setText("");
        mailFXHTMLSubjectField.setText("");
        mailFXHTMLEditor.setHtmlText("");
    }

    public void emailReply(AdvancedEmail mail){
        mailFXHTMLToField.setText(mail.Email.from().toString());
        mailFXHTMLCcField.setText(mail.Email.cc().toString());
        mailFXHTMLSubjectField.setText(mail.Email.subject());
        mailFXHTMLEditor.setHtmlText("");
        this.setFocusOff();
    }
    
    public void emailForward(AdvancedEmail mail){
        this.setFocusOff();
        this.makeHtmlLock();
        mailFXHTMLToField.setDisable(false);
    }

    public void makeHtmlLock(){
        mailFXHTMLEditor.setDisable(true);
    }
    
    public void makeHtmlUnlock(){
        mailFXHTMLEditor.setDisable(false);
    }

    public void saveAsADraft() throws IOException{
        String subject = mailFXHTMLSubjectField.getText();
        String tos;
        String ccs;
        String bccs;
        
        String[] toAddresses;
        String[] ccAddresses;
        String[] bccAddresses;
        
        propMan.load(config, "", "config");
        crud = new sqlOperations(config);
        
        if (!mailFXHTMLToField.getText().isEmpty()){
            tos = mailFXHTMLToField.getText();
            tos = tos.substring(0, tos.length());
            toAddresses = tos.split(",");
        }
        else { toAddresses = new String[0]; }
        
        if (!mailFXHTMLCcField.getText().isEmpty()){
            ccs = mailFXHTMLCcField.getText();
            ccs = ccs.substring(0, ccs.length());
            ccAddresses = ccs.split(",");
        }
        else { ccAddresses = new String[0]; }
        
        
        if (!mailFXHTMLBccField.getText().isEmpty()){
            bccs = mailFXHTMLBccField.getText();
            bccs = bccs.substring(0, bccs.length());
            bccAddresses = bccs.split(",");
        }
        else { bccAddresses = new String[0]; }
    
        ArrayList<File> files = new ArrayList<File>();
        for (File file : fileToAdd){
            files.add(file);
        }
        
        AdvancedEmail mail = new AdvancedEmail();
        Email email = new Email();
        email.subject(subject);
        email.to(toAddresses);
        email.cc(ccAddresses);
        email.bcc(bccAddresses);
        email.from("");
        EmailMessage message = new EmailMessage(mailFXHTMLEditor.getHtmlText(), "");
        email.message(message);
        
        mail.Email = email;
        mail.setFolderID(4);
        
        crud.addEmail(mail);
        
        this.setAllTextFieldsBlank();
    }
    
}

