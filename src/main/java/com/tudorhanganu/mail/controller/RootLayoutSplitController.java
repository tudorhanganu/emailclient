/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.mail.controller;

import com.tudorhanganu.application.MainApp;
import com.tudorhanganu.properties.PropertiesManager;
import javafx.event.ActionEvent;
import java.io.IOException;
import javafx.application.Platform;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Tudor Hanganu
 */
public class RootLayoutSplitController {
    
    private final static Logger LOG = LoggerFactory.getLogger(RootLayoutSplitController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="rootAnchorPane"
    private AnchorPane rootAnchorPane; // Value injected by FXMLLoader

    @FXML // fx:id="upperLeftSplit"
    private AnchorPane upperLeftSplit; // Value injected by FXMLLoader

    @FXML // fx:id="upperRightSplit"
    private AnchorPane upperRightSplit; // Value injected by FXMLLoader

    @FXML // fx:id="bottomSplit"
    private AnchorPane bottomSplit; // Value injected by FXMLLoader
    
    private MailFXTreeController treeController;
    private MailFXTableController tableController;
    private MailFXHTMLController htmlController;
    
    @FXML
    private MenuItem aboutMenu;

    @FXML
    void displayHelp(ActionEvent event) {
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(MainApp.primaryStage);
        dialog.setTitle("Help");
        VBox dialogVbox = new VBox(20);
        dialogVbox.getChildren().add(new Text("How to work: "));
        dialogVbox.getChildren().add(new Text("1. To send email, press Send new Email button, will enable all fields to be edited"));
        dialogVbox.getChildren().add(new Text("2. You can delete an email by pressing delete once one is selected"));
        dialogVbox.getChildren().add(new Text("3. You can reply, forward emails by selecting an email, and pressing the prefered option button"));
        dialogVbox.getChildren().add(new Text("4. Save to draft by starting a new email, and pressing save as draft."));
        Scene dialogScene = new Scene(dialogVbox, 600, 500);
        dialog.setScene(dialogScene);
        dialog.show();
    }
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
            
            initUpperLeftLayout();
            initUpperRightLayout();
            initBottomLayout();
            
            treeController.displayTree();
            tableController.displayTheTable();
        }
    
    private void initUpperLeftLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutSplitController.class.getResource("/fxml/mailFXTreeView.fxml"));
            AnchorPane treeView = (AnchorPane) loader.load();

            // Give the controller the data object.
            treeController = loader.getController();
            
            treeController.setController(this);
            
            upperLeftSplit.getChildren().add(treeView);
        } catch (IOException ex) {
            LOG.error("initUpperLeftLayout error", ex);
            errorAlert("initUpperLeftLayout()");
            Platform.exit();
        }
    }
    
    private void initUpperRightLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutSplitController.class.getResource("/fxml/mailFXTableView.fxml"));
            AnchorPane tableView = (AnchorPane) loader.load();

            // Give the controller the data object.
            tableController = loader.getController();
            
            tableController.setController(this);

            upperRightSplit.getChildren().add(tableView);
        } catch (IOException ex) {
            LOG.error("initUpperRightLayout error", ex);
            errorAlert("initUpperRightLayout()");
            Platform.exit();
        }
    }
    
    private void initBottomLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutSplitController.class.getResource("/fxml/mailFXHTMLLayoutView.fxml"));
            AnchorPane htmlView = (AnchorPane) loader.load();

            // Give the controller the data object.
            htmlController = loader.getController();

            bottomSplit.getChildren().add(htmlView);
        } catch (IOException ex) {
            LOG.error("initLowerRightLayout error", ex);
            errorAlert("initLowerRightLayout()");
            Platform.exit();
        }
    }
    
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("sqlError"));
        dialog.setHeaderText(resources.getString("sqlError"));
        dialog.setContentText(resources.getString(msg));
        dialog.show();
    }
    
    /**
     * returns instance of the html controller
     *      
     * @return
     */
    public MailFXHTMLController returnHTMlController(){
        return htmlController;
    }

    /**
     * returns instance of the split controller
     * @return
     */
    public RootLayoutSplitController returnSplitController(){
        return this;
    }

    /**
     * returns instance of the table controller
     * 
     * @return
     */
    public MailFXTableController returnTableController(){
        return this.tableController;
    }
    
    @FXML
    private MenuItem editPropMenu;

    @FXML
    void editProperties(ActionEvent event) {
        MainApp app = new MainApp();
        app.initPropertiesLayout();
    }

}


    
    

