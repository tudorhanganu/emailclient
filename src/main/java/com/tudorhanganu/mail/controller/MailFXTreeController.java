package com.tudorhanganu.mail.controller;

/**
 *
 * @author Tudor Hanganu
 */


import com.tudorhanganu.business.sqlOperations;
import com.tudorhanganu.config.ConfigBean;
import com.tudorhanganu.data.AdvancedEmail;
import com.tudorhanganu.data.FakeData;
import com.tudorhanganu.exceptions.InvalidEmailException;
import com.tudorhanganu.javafxbeans.FolderBean;
import com.tudorhanganu.properties.PropertiesManager;
import java.io.IOException;

import javafx.scene.control.TreeItem;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Tudor Hanganu
 */
public class MailFXTreeController {

    ConfigBean config = new ConfigBean();
    
    sqlOperations crud;
    
    PropertiesManager propMan = new PropertiesManager();
    
    RootLayoutSplitController controller;
    
    @FXML // fx:id="mailFXTreeLayout"
    private AnchorPane mailFXTreeLayout; // Value injected by FXMLLoader

    @FXML // fx:id="mailFXTreeView"
    private TreeView<FolderBean> mailFXTreeView; // Value injected by FXMLLoader

    @FXML
    private Button addFolderBtn;

    @FXML
    private Button deleteFolderBtn;

    @FXML
    void addFolder(ActionEvent event) {
        TextInputDialog td = new TextInputDialog();
        td.setHeaderText("Enter new folder name");
        
        td.showAndWait();
        
        String ret = td.getEditor().getText();
        
        crud.addFoler(ret);
        
        this.displayTree();
    }

    @FXML
    void delFolder(ActionEvent event) {
        TextInputDialog td = new TextInputDialog();
        td.setHeaderText("Enter Folder Name to Delete");
        
        td.showAndWait();
        
        String ret = td.getEditor().getText();
        
        crud.deleteFolder(ret);
        
        this.displayTree();
    }

    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws IOException {
        propMan.load(config, "", "config");
        
        crud = new sqlOperations(config);
        
        FolderBean folder = new FolderBean();
        
        folder.setName("Folders");
        mailFXTreeView.setRoot(new TreeItem<FolderBean>(folder));
        mailFXTreeView.setCellFactory((e) -> new TreeCell<FolderBean>() {
            @Override
            protected void updateItem(FolderBean item, boolean empty) {
                super.updateItem(item, empty);
                super.setOnDragOver((DragEvent event)->onDragOver(event));
                super.setOnDragDropped((DragEvent event)->{
                    try {
                        onDragDropped(event);
                    } catch (InvalidEmailException ex) {
                        Logger.getLogger(MailFXTreeController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(MailFXTreeController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                if (item != null) {
                    setText(item.getName());
                    setGraphic(getTreeItem().getGraphic());
                } else {
                    setText("");
                    setGraphic(null);
                }
            }
        });
    }
    
    /**
     * Build the tree from the database
     *
     */
    public void displayTree() {
        
        ObservableList<FolderBean> beans = crud.getAllFolders();
        mailFXTreeView.getRoot().getChildren().clear();

        if (beans != null) {
            for (FolderBean fb : beans) {
                TreeItem<FolderBean> item = new TreeItem<>(fb);
                item.setGraphic(new ImageView(getClass().getResource("/images/folder.png").toExternalForm()));
                mailFXTreeView.getRoot().getChildren().add(item);
            }
        }

        mailFXTreeView.getRoot().setExpanded(true);

        mailFXTreeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showElementData(newValue));
    }

    /**
     * displays proper emails based on which folder is clicked
     *
     * @param fishData
     */
    private void showElementData(TreeItem<FolderBean> folderBean) {
        controller.returnHTMlController().setAllTextFieldsBlank();
        
        controller.returnTableController().setFocusOff();
        controller.returnTableController().setFocusOn();
        int folderid = folderBean.getValue().getFolderID();
        controller.returnTableController().displayFolderEmails(folderid);
    }

    /**
     * sets config object
     * 
     * @param config
     */
    public void setConfig(ConfigBean config){
        this.config = config;
    }

    /**
     *  sets root controller
     * 
     * @param rootLayout
     */
    public void setController(RootLayoutSplitController rootLayout){
        this.controller = rootLayout;
    }

    @FXML
    void onDragDetected(MouseEvent event) {
        
        System.out.println("FOLDER DRAG DETECTED");
        
        Dragboard board = mailFXTreeView.startDragAndDrop(TransferMode.ANY);
        
        ClipboardContent content = new ClipboardContent();
        content.putString(mailFXTreeView.getSelectionModel().getSelectedItem().getValue().toString());
        
        board.setContent(content);
        
        event.consume();
    }

    @FXML
    void onDragDropped(DragEvent event) throws InvalidEmailException, IOException {
        
        TreeCell<FolderBean> element = (TreeCell) event.getSource();
        
        boolean succ = false;
        
        Dragboard board = event.getDragboard();
        
        if (element.getItem().getFolderID()!= 0){
            AdvancedEmail email = crud.getEmailWithID(Integer.parseInt(board.getString()));
            
            email.setFolderID(element.getItem().getFolderID());
            crud.deleteEmail(Integer.parseInt(board.getString()));
            crud.addEmail(email);
            
            succ = true;
        }
        
        event.setDropCompleted(succ);
        event.consume();
        
    }

    @FXML
    void onDragOver(DragEvent event) {
        
        Dragboard board = event.getDragboard();
        
        if (board.hasString()){
            event.acceptTransferModes(TransferMode.MOVE);
        }
        
        event.consume();
    }
}

