package com.tudorhanganu.mail.controller;
/**
 *
 * @author Tudor Hanganu
 */

import com.tudorhanganu.config.ConfigBean;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.tudorhanganu.properties.PropertiesManager;
import com.tudorhanganu.application.MainApp;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Tudor Hanganu
 */
public class PropertiesController {
    
    private final static Logger LOG = LoggerFactory.getLogger(PropertiesController.class);
    
    private ConfigBean config;
    private PropertiesManager propManager;
    
    private RootLayoutSplitController root;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="userNameField"
    private TextField userNameField; // Value injected by FXMLLoader

    @FXML // fx:id="emailAddressField"
    private TextField emailAddressField; // Value injected by FXMLLoader

    @FXML // fx:id="mailPasswordField"
    private TextField mailPasswordField; // Value injected by FXMLLoader

    @FXML // fx:id="imapURLField"
    private TextField imapURLField; // Value injected by FXMLLoader

    @FXML // fx:id="smtpURLField"
    private TextField smtpURLField; // Value injected by FXMLLoader

    @FXML // fx:id="imapPortField"
    private TextField imapPortField; // Value injected by FXMLLoader

    @FXML // fx:id="smtpPortField"
    private TextField smtpPortField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlURLField"
    private TextField mysqlURLField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlDatabaseField"
    private TextField mysqlDatabaseField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlPortField"
    private TextField mysqlPortField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlUserField"
    private TextField mysqlUserField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlPasswordField"
    private TextField mysqlPasswordField; // Value injected by FXMLLoader

    @FXML
    void pressCancel(ActionEvent event) throws IOException {
        Scene scene = null;

        FXMLLoader loader = new FXMLLoader();
        loader.setResources(resources);

        loader.setLocation(RootLayoutSplitController.class.getResource("/fxml/rootLayout.fxml"));

        AnchorPane root = (AnchorPane) loader.load();

        scene = new Scene(root);

        MainApp.primaryStage.setScene(scene);
        MainApp.primaryStage.setTitle("Setup");
        MainApp.primaryStage.show();
    }

    @FXML
    void pressSave(ActionEvent event) throws IOException {
        propManager.save(config, "", "config");
        
        Scene scene = null;

        FXMLLoader loader = new FXMLLoader();
        loader.setResources(resources);

        loader.setLocation(RootLayoutSplitController.class.getResource("/fxml/rootLayout.fxml"));

        AnchorPane root = (AnchorPane) loader.load();

        scene = new Scene(root);

        MainApp.primaryStage.setScene(scene);
        MainApp.primaryStage.setTitle("Setup");
        MainApp.primaryStage.show();
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert userNameField != null : "fx:id=\"userNameField\" was not injected: check your FXML file 'propertiesFormView.fxml'.";
        assert emailAddressField != null : "fx:id=\"emailAddressField\" was not injected: check your FXML file 'propertiesFormView.fxml'.";
        assert mailPasswordField != null : "fx:id=\"mailPasswordField\" was not injected: check your FXML file 'propertiesFormView.fxml'.";
        assert imapURLField != null : "fx:id=\"imapURLField\" was not injected: check your FXML file 'propertiesFormView.fxml'.";
        assert smtpURLField != null : "fx:id=\"smtpURLField\" was not injected: check your FXML file 'propertiesFormView.fxml'.";
        assert imapPortField != null : "fx:id=\"imapPortField\" was not injected: check your FXML file 'propertiesFormView.fxml'.";
        assert smtpPortField != null : "fx:id=\"smtpPortField\" was not injected: check your FXML file 'propertiesFormView.fxml'.";
        assert mysqlURLField != null : "fx:id=\"mysqlURLField\" was not injected: check your FXML file 'propertiesFormView.fxml'.";
        assert mysqlDatabaseField != null : "fx:id=\"mysqlDatabaseField\" was not injected: check your FXML file 'propertiesFormView.fxml'.";
        assert mysqlPortField != null : "fx:id=\"mysqlPortField\" was not injected: check your FXML file 'propertiesFormView.fxml'.";
        assert mysqlUserField != null : "fx:id=\"mysqlUserField\" was not injected: check your FXML file 'propertiesFormView.fxml'.";
        assert mysqlPasswordField != null : "fx:id=\"mysqlPasswordField\" was not injected: check your FXML file 'propertiesFormView.fxml'.";

    }
    
    private void bindings(){
        Bindings.bindBidirectional(userNameField.textProperty(), this.config.getUsername());
        Bindings.bindBidirectional(emailAddressField.textProperty(), this.config.getEmailAddress());
        Bindings.bindBidirectional(mailPasswordField.textProperty(), this.config.getPassword());
        Bindings.bindBidirectional(imapURLField.textProperty(), this.config.getImapURL());
        Bindings.bindBidirectional(smtpURLField.textProperty(), this.config.getSmtpURL());
        Bindings.bindBidirectional(imapPortField.textProperty(), this.config.getImapPort());
        Bindings.bindBidirectional(smtpPortField.textProperty(), this.config.getSmtpPort());
        Bindings.bindBidirectional(mysqlURLField.textProperty(), this.config.getSqlURL());
        Bindings.bindBidirectional(mysqlDatabaseField.textProperty(), this.config.getDbName());
        Bindings.bindBidirectional(mysqlPortField.textProperty(), this.config.getDbPort());
        Bindings.bindBidirectional(mysqlUserField.textProperty(), this.config.getDbUsername());
        Bindings.bindBidirectional(mysqlPasswordField.textProperty(), this.config.getDbPassword());
    }

    /**
     * sets config object
     * 
     * @param config
     * @throws IOException
     */
    public void setConfig(ConfigBean config) throws IOException{
        this.config = config;
        this.propManager = new PropertiesManager();
        bindings();
        propManager.load(config, "", "config");
    }
    
    
}

