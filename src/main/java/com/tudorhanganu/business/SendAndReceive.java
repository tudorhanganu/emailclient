/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.business;

import com.tudorhanganu.config.ConfigBean;
import com.tudorhanganu.data.AdvancedEmail;
import com.tudorhanganu.exceptions.InvalidEmailException;
import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.sql.Timestamp;

import javax.activation.DataSource;
import javax.mail.Flags;

import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailFilter;
import jodd.mail.EmailMessage;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author Tudor Hanganu
 */
public class SendAndReceive  {
    
    //Logger variable
    private final static Logger LOG = LoggerFactory.getLogger(SendAndReceive.class);

   /**
   * THis is the send mail method, which takes care of gathering information about the users to send to, the sender and the content.
   * 
   * @param user a configBean, holding info such as host, email address and password. It is the sender information.
   * @param toField list of recipients
   * @param subject subject of the email.
   * @param content message in the email
   * @param htmlMessage embedded HTML message.
   * @param cc array of email addresses.
   * @param bcc array of email addresses.
   * @param attachments list of strings, holding names for files.
   * @param embeddedAttachments list of strings, holding names for files.
   */
    public Email sendEmail(ConfigBean user, String[] toField, String subject, String content,String htmlMessage, String[] cc, String[] bcc, ArrayList<File> attachments, ArrayList<File> embeddedAttachments) throws InvalidEmailException{ 
        
        SmtpServer smtpServer = MailServer.create()
                .ssl(true)
                .host(user.smtpUrl())
                .auth(user.email(), user.password())
                .buildSmtpMailServer();
        
        Email email = Email.create().from(user.email())
                                    .subject(subject)
                                    .textMessage(content);
        
        // Assigning the recipients' email(s)
        if (checkTo(toField)){ email.to(toField); }
        
        // The following are all of the optional elements of the email
        // HTML MESSAGE
        if (!htmlMessage.equals("") ){ email.htmlMessage(htmlMessage); }
        
        // CC & BCC
        if (checkCc(cc)){ 
            email.cc(cc); 
        }
        
        if (checkBcc(bcc)){ 
            email.bcc(bcc); 
        }
        
        // ATTACHMENTS
        
        if (checkAttachments(attachments)){
            for (File att : attachments){
                email.attachment(EmailAttachment.with().content(att));
            }
        }
        if (checkAttachments(embeddedAttachments)){
            for (File embAtt : embeddedAttachments){
                email.embeddedAttachment(EmailAttachment.with().content(embAtt));
            }
        }
        
        // TRY CATCH CLAUSE FOR SENDING OUT THE EMAIL
        try ( SendMailSession session = smtpServer.createSession()) {
            session.open();
            session.sendMail(email);
            return email;
        } 
        catch (Exception exception) {
            LOG.debug("Failed to send email", exception);
            return email;
        }
    }
    
    /**
   * Returns all unread emails from db
   * 
   * @param user Holds information about the email address of the person we wish to retrieve emails from.
   * 
   * @return Email list, holding all the unread emails from that same account.
   * 
   */
    public ArrayList<ReceivedEmail> receiveEmail(ConfigBean user) {

        if (checkEmail(user.email())){
            // Creating IMAP server
            ImapServer imapServer = MailServer.create()
                .host(user.imapUrl())
                .ssl(true)
                .auth(user.email(), user.password())
                .buildImapMailServer();
            
            try ( ReceiveMailSession session = imapServer.createSession()) {
                
                session.open();
                
                ReceivedEmail[] emails = session.receiveEmailAndMarkSeen(EmailFilter.filter().flag(Flags.Flag.SEEN, false));
                ArrayList<ReceivedEmail> receivedEmails = new ArrayList<ReceivedEmail>();
                
                if (emails != null){
                    for (ReceivedEmail mail : emails){
                        
                        // Creating email object for each iteration
                        ReceivedEmail toAdd = ReceivedEmail.create();
                        
                        Timestamp time = new Timestamp(System.currentTimeMillis());
                        
                        toAdd.receivedDate(time);
                        
                        // Assigning subject field
                        toAdd.subject(mail.subject());
                        
                        // Assigning messages
                        List<EmailMessage> messages = mail.messages();
                        toAdd.message(messages);
                        
                        // Assigning from field
                        toAdd.from(mail.from().getEmail());
                        
                        // Assigning to field
                        for (EmailAddress mailAddress : mail.to()) {
                            toAdd.to(mailAddress);
                        }
                        
                        // Assigning cc field
                        for (EmailAddress mailAddress : mail.cc()) {
                            toAdd.cc(mailAddress);
                        }
                        
                        // Assigning attachments (embedded or not)
                        List<EmailAttachment<? extends DataSource>> attachments = mail.attachments();
                        for (EmailAttachment attachment : attachments){
                            toAdd.attachment(attachment);
                        }
                        
                        receivedEmails.add(toAdd);
                    }
                    
                    return receivedEmails;
                }
            }
            catch (Exception exception){
                LOG.debug("Error in receiving emails");
            }
        }
        
        return null;
    }
    
   /**
   * THis is the send mail method, which takes care of gathering information about the users to send to, the sender and the content.
   * 
   * @param address email address to check
   * 
   * @return boolean, wether the email address is valid or no
   */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }
    
   /**
   * This method is used to check cc addresses used in a specific email, and if one is incorrect, will throw an appropriate exception
   * 
   * @param cc String array of cc email addresses
   * 
   * @return boolean, wether the emails are valid or no
   */
    private boolean checkCc(String[] cc) throws InvalidEmailException{
        for (String email : cc){
            if (!checkEmail(email)){
                throw new InvalidEmailException("Wrong CC email: " + email);
            }
        }
        return true;
    }
    
   /**
   * This method is used to check bcc addresses used in a specific email, and if one is incorrect, will throw an appropriate exception
   * 
   * @param cc String array of bcc email addresses
   * 
   * @return boolean, wether the emails are valid or no
   */
    private boolean checkBcc(String[] bcc) throws InvalidEmailException{
        for (String email : bcc){
            if (!checkEmail(email)){
                throw new InvalidEmailException("Wrong BCC email: " + email);
            }
        }
        return true;
    }
    
     /**
   * This method is used to check to addresses used in a specific email, and if one is incorrect, will throw an appropriate exception
   * 
   * @param cc List of strings of to email addresses
   * 
   * @return boolean, wether the emails are valid or no
   */
    private boolean checkTo(String[] to) throws InvalidEmailException{
        for (String email : to){
            if (!checkEmail(email)){
                throw new InvalidEmailException("Wrong TO email: " + email);
            }
        }
        return true;
    }

    private boolean checkAttachments(ArrayList<File> files) throws InvalidEmailException{
        for (File file  : files){
            if (!file.isFile()){
                
                throw new InvalidEmailException("One or more attachments are invalid");
            }
        }
        return true;
    }
    
}
