/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.business;

import com.tudorhanganu.config.ConfigBean;
import com.tudorhanganu.data.AdvancedEmail;
import com.tudorhanganu.exceptions.InvalidEmailException;
import com.tudorhanganu.javafxbeans.FolderBean;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.activation.DataSource;
import static jodd.io.FileUtil.file;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailAttachmentBuilder;
import jodd.mail.ReceivedEmail;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Tudor Hanganu
 */
public class sqlOperations {
    
    // variables holding the information for the database connection
    private String sqlUrl;
    private String dbUsername;
    private String dbPassword;
    
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(sqlOperations.class);

    /**
     *  CRUD constructor, will be used to call the methods and hold information for connecting to the DB
     * 
     * @param configbean
     */
    public sqlOperations(ConfigBean configbean) {
        
        if (configbean.sqlurl().equals("localhost")){
             String url = ("jdbc:mysql://" + configbean.sqlurl() + ":" + configbean.sqlPort() + "/" + configbean.dbname());
             this.sqlUrl = url;
        }
        else {
            this.sqlUrl = configbean.sqlurl();
        }
        
        this.dbUsername = configbean.dbuser();
        
        this.dbPassword = configbean.dbpass();
        
    }

    /**
     * Method to get all emails from the DB
     * 
     * @return An observableList of advancedemails, holding all the emails in the emails table.
     */
    public ObservableList<AdvancedEmail> getAllEmails() {
        
        try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "SELECT * FROM Email";
            
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery(sqlQuery);
            
            ObservableList<AdvancedEmail> output = FXCollections.observableArrayList();
            
            while (result.next()){
                //LOG.info(result.getString(2));
                output.add(createEmail(result));
            }
            return output;
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return null;
        }
    }

    /**
     *Method to get all emails in a specific folder.
     * 
     * @param folder ID of the folder to search in
     * @return an observableList of the emails in the folder
     */
    public ObservableList<AdvancedEmail> getAllEmails(int folder){
        
        try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "SELECT * FROM email";
            
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery(sqlQuery);
            
            ObservableList<AdvancedEmail> output = FXCollections.observableArrayList();
            
            while (result.next()){
                if (result.getInt("folderid")==folder){
                    output.add(createEmail(result));
                }
            }
            return output;
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return null;
        }
    }
    
    /**
     * Method to get a specific email, with id
     * 
     * @param emailid the id of the email
     * @return the email object
     * @throws com.tudorhanganu.exceptions.InvalidEmailException
     */
    public AdvancedEmail getEmailWithID(int emailid) throws InvalidEmailException{
        
        try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "SELECT * FROM Email";
            
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery(sqlQuery);
            
            AdvancedEmail output = new AdvancedEmail();
            
            while (result.next()){
                if (result.getInt(1)==emailid){
                    output = createEmail(result);
                }
            }
            return output;
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return null;
        }
    }

    /**
     *  Returns all folders in the db
     * 
     * @return observableList of folderBEan
     */
    public ObservableList<FolderBean> getAllFolders(){
        try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "SELECT * FROM Folder";
            
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery(sqlQuery);
            
            ObservableList<FolderBean> output = FXCollections.observableArrayList();
            
            while (result.next()){
                FolderBean bean = new FolderBean();
                bean.setFolderID(result.getInt(1));
                bean.setName(result.getString(2));
                output.add(bean);
            }
            return output;
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return null;
        }
    }
    
    /**
     *Method to delete specific email
     * 
     * @param emailID id of email to delete
     */
    public void deleteEmail(int emailID){
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "DELETE FROM email WHERE emailID = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setInt(1, emailID);
            
            statement.executeUpdate();
            
            this.deleteFromCc(emailID);
            this.deleteFromBcc(emailID);
            this.deleteFromTo(emailID);
            this.deleteFromLinkedTables(emailID);
            this.deleteFromAttachments(emailID);
            this.deleteFromAttachmentLink(emailID);
        }
        catch (SQLException ex){
            ex.printStackTrace();
        }
        
    }
    
    /**
     * method to delete specific folder
     * 
     * @param name
     */
    public void deleteFolder(String name){
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
             this.deleteFolderContent(name);
             
            String sqlQuery = "DELETE FROM folder WHERE name = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setString(1, name);
            statement.executeUpdate();
            
        }
        catch (SQLException ex){
            ex.printStackTrace();
            
        }
    }

    /**
     *Method to add an email to the DB, and all of its related attributes, cc,bcc,to, etc.in other tables
     * 
     * @param email the email to add to the DB
     * @throws java.io.IOException
     */
    public void addEmail(AdvancedEmail email) throws IOException{
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "INSERT INTO email (fromfield, subjectfield, message, htmlmessage, sentdate, receiveddate, folderid) VALUES (?,?,?,?,?,?,?)";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            this.fillEmailPreparedStatement(statement, email);
            
            statement.executeUpdate();
            
            ArrayList<Integer> ccids = new ArrayList<Integer>();
            ArrayList<Integer> bccids = new ArrayList<Integer>();
            ArrayList<Integer> toids = new ArrayList<Integer>();
            
            for (EmailAddress ccemail : email.Email.cc()){
                 ccids.add(this.insertAddresses(ccemail));
            }
            for (EmailAddress bccemail : email.Email.bcc()){
                 bccids.add(this.insertAddresses(bccemail));
            }
            for (EmailAddress toemail : email.Email.to()){
                 toids.add(this.insertAddresses(toemail));
            }
            
            int ccid = insertCc(ccids);
            int bccid = insertBcc(bccids);
            int toid = insertTo(toids);
            
            String idQuery = "SELECT LAST_INSERT_ID()";
            
            Statement idStatement = conn.prepareStatement(idQuery);
            ResultSet rs = idStatement.executeQuery(idQuery);
            
            int emailID = -1;
            
            if (rs.next()){
                emailID = rs.getInt(1);
            }
            
            insertEmailLinks(emailID, ccid, bccid, toid);
            
            int attachmentID;
            
            for (EmailAttachment attachment : email.Email.attachments()){
                attachmentID = insertAttachment(attachment);
                insertAttachmentLink(emailID, attachmentID);
                
            }
        }
        catch (SQLException ex){
            ex.printStackTrace();
            
        }
        
    }
    
    /**
     *Method to add a folder to the DB
     * 
     * @param folderName name of the folder to create
     */
    public void addFoler(String folderName){
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "INSERT INTO folder(name) VALUES (?)";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setString(1, folderName);
           
            statement.executeUpdate();
          
        }
        catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Method to edit an email, changing the folder it is in.
     * 
     * @param emailID id of email to change
     * @param folderID id of folder to put in
     */
    public void editEmail(int emailID, int folderID){
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "UPDATE EMAIL SET folderid = ? WHERE emailid = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            
            statement.setInt(1, folderID);
            statement.setInt(2, emailID);
           
            statement.executeUpdate();
          
        }
        catch (SQLException ex){
            ex.printStackTrace();
        }
    }
    
    /**
     *  Method to change folder name
     * 
     * @param folderID id of folder 
     * @param newFolderName new name
     */
    public void editFolder(int folderID, String newFolderName){
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "UPDATE FOLDER SET name = ? WHERE folderid = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            
            statement.setString(1, newFolderName);
            statement.setInt(2, folderID);
           
            statement.executeUpdate();
          
        }
        catch (SQLException ex){
            ex.printStackTrace();
        }
    }
    
    /**
     *  Fetches all to addresses of an email
     * 
     * @param emailID
     * @return string array of to addresses
     */
    public String[] getToAddresses(int emailID){
        
        try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)){
            String sqlQuery = "select addresses.address from addresses\n" +
                                        "inner join toaddresses on addresses.addressID = toaddresses.addressID\n" +
                                        "inner join addresslink on toaddresses.toID = addresslink.toID\n" +
                                        "inner join email on addresslink.emailID = email.emailID\n" +
                                        "where email.emailID = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setInt(1, emailID);
            
            ResultSet result = statement.executeQuery();
            
            ArrayList<String> toAddresses = new ArrayList<String>();
            
            while (result.next()){
                String to = result.getString(1);
                toAddresses.add(to);
            }
            
            String[] toAddressesArray = toAddresses.toArray(new String[toAddresses.size()]);
            
            return toAddressesArray;
            
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     *  returns all cc addresses of an email
     * 
     * @param emailID
     * @return string array
     */
    public String[] getCcAddresses(int emailID){
        
        try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)){
            String sqlQuery = "select addresses.address from addresses\n" +
                                        "inner join ccaddresses on addresses.addressID = ccaddresses.addressID\n" +
                                        "inner join addresslink on ccaddresses.ccID = addresslink.ccId\n" +
                                        "inner join email on addresslink.emailID = email.emailID\n" +
                                        "where email.emailID = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setInt(1, emailID);
            
            ResultSet result = statement.executeQuery();
            
            ArrayList<String> ccAddresses = new ArrayList<String>();
            
            while (result.next()){
                String cc = result.getString(1);
                ccAddresses.add(cc);
            }
            
            String[] ccAddressesArray = ccAddresses.toArray(new String[ccAddresses.size()]);
            
            return ccAddressesArray;
            
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * fetches all bcc addresses of specific email
     * 
     * @param emailID
     * @return string array
     */
    public String[] getBccAddresses(int emailID){
        
        try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)){
            String sqlQuery = "select addresses.address from addresses\n" +
                                        "inner join bccaddresses on addresses.addressID = bccaddresses.addressID\n" +
                                        "inner join addresslink on bccaddresses.bccID = addresslink.bccId\n" +
                                        "inner join email on addresslink.emailID = email.emailID\n" +
                                        "where email.emailID = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setInt(1, emailID);
            
            ResultSet result = statement.executeQuery();
            
            ArrayList<String> bccAddresses = new ArrayList<String>();
            
            while (result.next()){
                String bcc = result.getString(1);
                bccAddresses.add(bcc);
            }
            
            String[] bccAddressesArray = bccAddresses.toArray(new String[bccAddresses.size()]);
            
            return bccAddressesArray;
            
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Converts received email into emailBean
     * 
     * @param rec
     * @return advancedEmail object
     */
    public AdvancedEmail convertEmail(ReceivedEmail rec){
        AdvancedEmail email = new AdvancedEmail();
        email.Email.from(rec.from());
        email.Email.to(rec.to());
        email.Email.subject(rec.subject());
        email.Email.message(rec.messages().get(0));
        email.Email.htmlMessage("Blank");
        email.Email.cc(rec.cc());
        
        email.Email.attachments(rec.attachments());
        
        LocalDate now = LocalDate.now();
        
        email.setReceivedDate(Date.valueOf(now));
        
        email.setFolderID(2);
        
        return email;
    }
    
    private ArrayList<EmailAttachment> getAttachments(int emailID){
        
        try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)){
            String sqlQuery = "select attachments.cid, attachments.isEmbedded, attachments.attachname, attachments.attachedFile from email\n" +
                                        "inner join emailattachments on email.emailid = emailattachments.emailID\n" +
                                        "inner join attachments on emailattachments.attachmentID = attachments.attachmentID\n" +
                                        "where email.emailID = ?";
            
            PreparedStatement statement = conn.prepareCall(sqlQuery);
            statement.setInt(1, emailID);
            
            ResultSet result = statement.executeQuery();
            
            ArrayList<EmailAttachment> attachments = new ArrayList<EmailAttachment>();
            
            while (result.next()){
                int isEmbedded = result.getInt(2);
                if (isEmbedded == 0){
                    String cid = result.getString(1);
                    String name = result.getString(3);
                    Blob blob = result.getBlob(4);
                    
                    EmailAttachment attach;
                    
                    if (blob != null){
                        int blobLength = (int) blob.length();
                        byte[] blobAsBytes = blob.getBytes(1, blobLength);
                        
                        EmailAttachmentBuilder co = EmailAttachment.with();
                        co.name(name);
                        co.contentId(cid);
                        co.inline(false);
                        co.content(blobAsBytes);
                        
                        attach = co.buildByteArrayDataSource();
                    }
                    else {
                        byte[] blobAsBytes = new byte[0];

                        EmailAttachmentBuilder co = EmailAttachment.with();
                        co.name(name);
                        co.contentId(cid);
                        co.inline(false);
                        co.content(blobAsBytes);
                        
                        attach = co.buildByteArrayDataSource();
                    }
                    
                    attachments.add(attach);
                }
            }
            return attachments;
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private ArrayList<EmailAttachment> getEmbeddedAttachments(int emailID){
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)){
            String sqlQuery = "select attachments.cid, attachments.isEmbedded, attachments.attachname, attachments.attachedFile from email\n" +
                                        "inner join emailattachments on email.emailid = emailattachments.emailID\n" +
                                        "inner join attachments on emailattachments.attachmentID = attachments.attachmentID\n" +
                                        "where email.emailID = ?";
            
            PreparedStatement statement = conn.prepareCall(sqlQuery);
            statement.setInt(1, emailID);
            
            ResultSet result = statement.executeQuery();
            
            ArrayList<EmailAttachment> attachments = new ArrayList<EmailAttachment>();
            
            while (result.next()){
                int isEmbedded = result.getInt(2);
                if (isEmbedded == 1){
                    String cid = result.getString(1);
                    String name = result.getString(3);
                    Blob blob = result.getBlob(4);
                    
                    EmailAttachment attach;
                    
                    if (blob != null){
                        int blobLength = (int) blob.length();
                        byte[] blobAsBytes = blob.getBytes(1, blobLength);
                        
                        EmailAttachmentBuilder co = EmailAttachment.with();
                        co.name(name);
                        co.contentId(cid);
                        co.inline(false);
                        co.content(blobAsBytes);
                        
                        attach = co.buildByteArrayDataSource();
                    }
                    else {
                        byte[] blobAsBytes = new byte[0];

                        EmailAttachmentBuilder co = EmailAttachment.with();
                        co.name(name);
                        co.contentId(cid);
                        co.inline(false);
                        co.content(blobAsBytes);
                        
                        attach = co.buildByteArrayDataSource();
                    }
                    
                    attachments.add(attach);
                }
            }
            return attachments;
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
            
        
    }
    
    private AdvancedEmail createEmail(ResultSet resultSet) throws SQLException{
        int emailID = resultSet.getInt(1);
        String from = resultSet.getString(2);
        String subject = resultSet.getString(3);
        String message = resultSet.getString(4);
        String htmlMessage = resultSet.getString(5);
        Date sentDate = resultSet.getDate(6);
        Date receivedDate = resultSet.getDate(7);
        int folderID = resultSet.getInt(8);
        
        AdvancedEmail advEmail = new AdvancedEmail(emailID, folderID, sentDate, receivedDate);
        advEmail.Email.from(from);
        advEmail.Email.textMessage(message);
        advEmail.Email.subject(subject);
        advEmail.Email.htmlMessage(htmlMessage);
        advEmail.Email.to(this.getToAddresses(emailID));
        advEmail.Email.cc(this.getCcAddresses(emailID));
        advEmail.Email.bcc(this.getBccAddresses(emailID));
        for (EmailAttachment f : this.getAttachments(emailID)){
            advEmail.Email.attachment(f);
        }
        for (EmailAttachment f : this.getEmbeddedAttachments(emailID)){ 
            advEmail.Email.embeddedAttachment(f);  
        }
        
        return advEmail;
    }

    private void fillEmailPreparedStatement(PreparedStatement prepS, AdvancedEmail advEmail) throws SQLException{
        if (!advEmail.Email.from().getEmail().equals("") ){ 
            prepS.setString(1, advEmail.Email.from().getEmail()); 
        } 
        else {
            prepS.setString(1, "");
        }
        if (!advEmail.Email.subject().equals("")){
            prepS.setString(2, advEmail.Email.subject());
        } 
        else {
            prepS.setString(2, "");
        }
        if (advEmail.Email.messages().size()>0){
            if (!advEmail.Email.messages().get(0).getContent().equals("")) {
                prepS.setString(3, advEmail.Email.messages().get(0).getContent());
        } 
            else {
                prepS.setString(3, "");
        }
        }
        
        if (advEmail.Email.messages().size() > 1){
             if (!advEmail.Email.messages().get(1).getContent().equals("")) {
                 prepS.setString(4, advEmail.Email.messages().get(1).getContent());
             } 
        }
        else {
            prepS.setString(4, "");
        }
        if (advEmail.getSentDate() == null) {
            prepS.setDate(5, null);
        }
        else {
            prepS.setDate(5, (java.sql.Date) advEmail.getSentDate());
        }
        if (advEmail.getReceivedDate() == null) {
            prepS.setDate(6, null);
        } 
        else  {
            prepS.setDate(6, (java.sql.Date) advEmail.getReceivedDate());
        }
        
        prepS.setInt(7, advEmail.getFolderID());
    }
    
    private void deleteFromCc(int emailID) throws SQLException{
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "SELECT ccID FROM addresslink WHERE emailID = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setInt(1, emailID);
            //statement.executeUpdate(sqlQuery);
            
            ResultSet result = statement.executeQuery();
            
            int ccTemp = 0;
            
            while (result.next()){
                ccTemp = result.getInt(1);
            }
            
            sqlQuery = "DELETE FROM ccAddresses WHERE ccID = ?";
            statement = conn.prepareStatement(sqlQuery);
            statement.setInt(1, ccTemp);
            
            statement.executeUpdate();
        }
        catch (SQLException ex){
            ex.printStackTrace();
            
        }
         
        
    }
    
    private void deleteFromBcc(int emailID){
        try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "SELECT bccID FROM addresslink WHERE emailID = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setInt(1, emailID);
            //statement.executeUpdate(sqlQuery);
            
            ResultSet result = statement.executeQuery();
            
            int bccTemp = 0;
            
            while (result.next()){
                bccTemp = result.getInt(1);
            }
            
            sqlQuery = "DELETE FROM bccAddresses WHERE bccID = ?";
            statement = conn.prepareStatement(sqlQuery);
            statement.setInt(1, bccTemp);
            
            statement.executeUpdate();
        }
        catch (SQLException ex){
            ex.printStackTrace();
            
        }
    }
    
    private void deleteFromTo(int emailID){
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "SELECT toID FROM addresslink WHERE emailID = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setInt(1, emailID);
            //statement.executeUpdate(sqlQuery);
            
            ResultSet result = statement.executeQuery();
            
            int toTemp = 0;
            
            while (result.next()){
                toTemp = result.getInt(1);
            }
            
            sqlQuery = "DELETE FROM toAddresses WHERE toID = ?";
            statement = conn.prepareStatement(sqlQuery);
            statement.setInt(1, toTemp);
            
            statement.executeUpdate();
        }
        catch (SQLException ex){
            ex.printStackTrace();
            
        }
    }
    
    private void deleteFromLinkedTables(int emailID){
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "DELETE FROM addresslink WHERE emailID = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setInt(1, emailID);
            statement.executeUpdate();
        }
        catch (SQLException ex){
            ex.printStackTrace();
            
        }
    }
    
    private void deleteFromAttachmentLink(int emailID){
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "DELETE FROM emailattachments WHERE emailID = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setInt(1, emailID);
            statement.executeUpdate();
        }
        catch (SQLException ex){
            ex.printStackTrace();
            
        }
    }
    
    private void deleteFromAttachments(int emailID){
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "SELECT attachmentID FROM emailattachments WHERE emailID = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setInt(1, emailID);
            
            ResultSet result = statement.executeQuery();
            
            ArrayList<Integer> attIDs = new ArrayList<Integer>();
            
            while (result.next()){
                attIDs.add(result.getInt(1));
            }
            
            sqlQuery = "DELETE FROM attachments WHERE attachmentID = ?";
            
            PreparedStatement statementTwo = conn.prepareStatement(sqlQuery);
            
            for (int attID : attIDs){
                 statementTwo.setInt(1, attID);
                 statementTwo.executeUpdate();
            }
        }
        catch (SQLException ex){
            ex.printStackTrace();
            
        }
    }

    private void deleteFolderContent(String name){
        try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            
            String sqlQuery = "SELECT folderID FROM folder WHERE name = ?";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setString(1, name);
            ResultSet rs = statement.executeQuery();
            
            int id = 0;
            
            while (rs.next()){
                id = rs.getInt(1);
            }
            
            String secondQuery = "DELETE FROM email WHERE folderID = ?";
            
            PreparedStatement stat = conn.prepareStatement(secondQuery);
            stat.setInt(1, id);
            stat.executeUpdate();
            
        }
        catch (SQLException ex){
            ex.printStackTrace();
            
        }
    }
    
    private int insertAddresses(EmailAddress address) throws SQLException{
        int addressID;
        
        try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "SELECT addressid, address FROM addresses";
            
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery(sqlQuery);
            
            addressID = -1;
            
            boolean status = false;
            
            while (result.next()){
                if (result.getString(2).trim().equals(address.getEmail().trim())){
                    status = true;
                    addressID = result.getInt(1);
                }
                 
            }
          
            result.close();
            
            if (!status){
                String insertQuery = "INSERT INTO addresses (address) VALUES (?)";
                PreparedStatement insertStatement = conn.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
                
                insertStatement.setString(1, address.getEmail());
                insertStatement.executeUpdate();
                
                ResultSet rs = insertStatement.getGeneratedKeys();
                
                if (rs.next()){
                    addressID = rs.getInt(1);
                }
            
            return addressID;
            }
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return -1;
        }
        return addressID;
    }
        
    private int insertCc(ArrayList<Integer> ids){
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "INSERT INTO ccaddresses(addressID) VALUES (?)";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            
            for (int id : ids){
                statement.setInt(1, id);
                statement.executeUpdate();
            }
            
            String returnQuery = "SELECT LAST_INSERT_ID()";
            Statement returnStatement = conn.prepareCall(returnQuery);
            
            ResultSet rs = returnStatement.executeQuery(returnQuery);
            
            while (rs.next()){
                return rs.getInt(1);
            }
            return 0;
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return 0;
        }
    }
    
    private int insertBcc(ArrayList<Integer> ids){
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "INSERT INTO bccaddresses(addressID) VALUES (?)";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            
            for (int id : ids){
                statement.setInt(1, id);
                statement.executeUpdate();
            }
            String returnQuery = "SELECT LAST_INSERT_ID()";
            Statement returnStatement = conn.prepareCall(returnQuery);
            
            ResultSet rs = returnStatement.executeQuery(returnQuery);
            
            while (rs.next()){
                return rs.getInt(1);
            }
            return 0;
            
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return 0;
        }
    }
    
    private int insertTo(ArrayList<Integer> ids){
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "INSERT INTO toaddresses(addressID) VALUES (?)";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            
            for (int id : ids){
                statement.setInt(1, id);
                statement.executeUpdate();
            }
            String returnQuery = "SELECT LAST_INSERT_ID()";
            Statement returnStatement = conn.prepareCall(returnQuery);
            
            ResultSet rs = returnStatement.executeQuery(returnQuery);
            
            while (rs.next()){
                return rs.getInt(1);
            }
            return 0;
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return 0;
        }
    }

    private void insertEmailLinks(int emailid, int ccid, int bccid, int toid){
        // check which ones are 0, if so, set to null
         try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "INSERT INTO addresslink(emailid, ccid, bccid, toid) VALUES (?,?,?,?)";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);

            if (emailid == 0){ statement.setInt(1, -1); }
            else { statement.setInt(1, emailid); }
            if (ccid == 0){ statement.setInt(2, -1); }
            else { statement.setInt(2, ccid); }
            if (bccid == 0){ statement.setInt(3, -1); }
            else { statement.setInt(3, bccid); }
            if (emailid == 0){ statement.setInt(4, -1); }
            else { statement.setInt(4, toid); }
            
           statement.executeUpdate();
          
        }
        catch (SQLException ex){
            ex.printStackTrace();
        }
    }
    
    private int insertAttachment(EmailAttachment attachment){
        try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "INSERT INTO attachments(attachname, cid, attachedfile, isembedded) VALUES (?,?,?,?)";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            
            if (attachment.getContentId() == null){
                statement.setString(1, attachment.getName());
                statement.setString(2, attachment.getName());
                statement.setBytes(3, attachment.toByteArray());
                statement.setBoolean(4 , attachment.isEmbedded());
            }
            else {
                statement.setString(1, attachment.getName());
                statement.setString(2, attachment.getContentId());
                statement.setBytes(3, attachment.toByteArray());
                statement.setBoolean(4 , attachment.isEmbedded());
            }
            
            statement.executeUpdate();
            
            String attachmentIDQuery = "SELECT LAST_INSERT_ID()";
            Statement returnStatement = conn.createStatement();

            ResultSet rs = returnStatement.executeQuery(attachmentIDQuery);

            int id = 0;
            
            while (rs.next()){
                id = rs.getInt(1);
            }
            
            return id;
          
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return 0;
        }
    }
    
    private void insertAttachmentLink(int emailid, int attachmentid){
        try (Connection conn = DriverManager.getConnection(this.sqlUrl, this.dbUsername, this.dbPassword)) {
            String sqlQuery = "INSERT INTO emailattachments(emailid, attachmentid) VALUES (?,?)";
            
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            
            statement.setInt(1, emailid);
            statement.setInt(2, attachmentid);
           
            statement.executeUpdate();
          
        }
        catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    
   
   
}
