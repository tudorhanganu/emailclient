/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.data;

import java.util.Date;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import jodd.mail.Email;

/**
 *
 * @author Tudor Hanganu
 */
public class AdvancedEmail extends Email {
    
    private int emailID;
    private int folderID;
    private Date sentDate;
    private Date receivedDate;
    public Email Email;
    
    /**
    * Default Costructor
    */
    public AdvancedEmail(){
        this.Email = new Email();
    }
    /**
    * Constructor to create an email object and add additional info associated to it. 
    * 
    * @param emailID the unique emailID
    * @param folderID the id for the folder to put the email in
    * @param receivedDate the date at which the email was received, can be set to null
    * @param email the email object holding all of the info
    */
    public AdvancedEmail(int emailID, int folderID, Date sentDate, Date receivedDate){
       this.emailID = emailID;
       this.folderID = folderID;
       this.sentDate = sentDate;
       this.receivedDate = receivedDate;
       this.Email = new Email();
    }

    public int getEmailID() {
        return emailID;
    }

    public void setEmailID(int emailID) {
        this.emailID = emailID;
    }

    public int getFolderID() {
        return folderID;
    }

    public void setFolderID(int folderID) {
        this.folderID = folderID;
    }

    public Date getSentDate() {
        return this.sentDate;
    }
    
    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }
    
    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }
    
    public StringProperty getFromField(){
        return new SimpleStringProperty(this.Email.from().getEmail());
    }
    
    public StringProperty getSubjectField(){
        return new SimpleStringProperty(this.Email.subject());
    } 
    
    public StringProperty getDateField(){
        if (this.receivedDate == null){
            return new SimpleStringProperty("");
        }
        return new SimpleStringProperty(this.receivedDate.toString());
    }
}
