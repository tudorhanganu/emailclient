/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tudorhanganu.javafxbeans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Tudor Hanganu
 */
public class FolderBean {
    
    private IntegerProperty folderID;
    private StringProperty name;
    
    /**
     * Default constructor
     */
    public FolderBean(){
        this(-1, "");
    }
    
    /**
     *  Coonstructor taking the folder id and the name 
     * 
     * @param folderID
     * @param name
     */
    public FolderBean(final int folderID, final String name){
        super();
        this.folderID = new SimpleIntegerProperty(folderID);
        this.name = new SimpleStringProperty(name);
    }

    public void setFolderID(int folderID) {
        this.folderID.set(folderID);
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public int getFolderID() {
        return folderID.get();
    }

    public String getName() {
        return name.get();
    }
    
    
    
}
