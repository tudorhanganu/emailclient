SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS Folder;
DROP TABLE IF EXISTS Email;
DROP TABLE IF EXISTS Attachments;
DROP TABLE IF EXISTS EmailAttachments;
DROP TABLE IF EXISTS Addresses;
DROP TABLE IF EXISTS ccAddresses;
DROP TABLE IF EXISTS bccAddresses;
DROP TABLE IF EXISTS toAddresses;
DROP TABLE IF EXISTS addressLink;
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE Folder (
    folderID int(4) NOT NULL auto_increment,
    name VARCHAR(10),
    PRIMARY KEY(folderID)
);

INSERT INTO Folder(folderID, name) values
(0001, "sent"),
(0002, "inbox"),
(0003, "spam"),
(0004, "draft");

CREATE TABLE Email (
    emailID int(4) NOT NULL AUTO_INCREMENT, 
    fromField VARCHAR(50),
    subjectField VARCHAR(40),
    message VARCHAR(500),
    htmlmessage VARCHAR(200),
    sentDate DATE,
    receivedDate DATE,
    folderID int(4) REFERENCES Folder.folderID,
    primary key(emailID)
);

INSERT INTO Email(emailID, fromField, subjectField, message, htmlmessage, sentDate, receivedDate, folderID) values 
(0001, 'tudorsendmail@gmail.com', 'Test Message 01', 'Hello, this is a test email sample 01', '<p>Html Message 01</p>', '2020-09-29', null, 0001),
(0002, 'tudorsendmail@gmail.com', 'Test Message 02', 'Hello, this is a test email sample 02', '<p>Html Message 02</p>', '2020-08-11', null, 0001),
(0003, 'tudorsendmail@gmail.com', 'Test Message 03', 'Hello, this is a test email sample 03', '<p>Html Message 03</p>', null, '2020-09-09', 0002),
(0004, 'tudorsendmail@gmail.com', 'Test Message 04', 'Hello, this is a test email sample 04', '<p>Html Message 04</p>', '2020-09-27', null, 0001),
(0005, 'tudorsendmail@gmail.com', 'Test Message 05', 'Hello, this is a test email sample 05', '<p>Html Message 05</p>', null, '2020-10-02', 0002),
(0006, 'tudorsendmail@gmail.com', 'Test Message 06', 'Hello, this is a test email sample 06', '<p>Html Message 06</p>', null, '2020-10-01', 0002),
(0007, 'tudorsendmail@gmail.com', 'Test Message 07', 'Hello, this is a test email sample 07', '<p>Html Message 07</p>', null, '2020-10-03', 0002),
(0008, 'tudorsendmail@gmail.com', 'Test Message 08', 'Hello, this is a test email sample 08', '<p>Html Message 08</p>', null, '2020-09-10', 0002),
(0009, 'tudorsendmail@gmail.com', 'Test Message 09', 'Hello, this is a test email sample 09', '<p>Html Message 09</p>', null, '2020-07-13', 0002),
(0010, 'tudorsendmail@gmail.com', 'Test Message 10', 'Hello, this is a test email sample 10', '<p>Html Message 10</p>', null, '2020-01-01', 0003),
(0011, 'tudorsendmail@gmail.com', 'Test Message 11', 'Hello, this is a test email sample 11', '<p>Html Message 11</p>', null, '2020-10-01', 0002),
(0012, 'tudorsendmail@gmail.com', 'Test Message 12', 'Hello, this is a test email sample 12', '<p>Html Message 12</p>', null, '2020-10-02', 0002),
(0013, 'tudorsendmail@gmail.com', 'Test Message 13', 'Hello, this is a test email sample 13', '<p>Html Message 13</p>', null, '2020-10-03', 0002),
(0014, 'tudorsendmail@gmail.com', 'Test Message 14', 'Hello, this is a test email sample 14', '<p>Html Message 14</p>', null, '2020-09-29', 0002),
(0015, 'tudorsendmail@gmail.com', 'Test Message 15', 'Hello, this is a test email sample 15', '<p>Html Message 15</p>', null, '2020-09-29', 0002),
(0016, 'tudorsendmail@gmail.com', 'Test Message 16', 'Hello, this is a test email sample 16', '<p>Html Message 16</p>', null, '2020-08-27', 0003),
(0017, 'tudorsendmail@gmail.com', 'Test Message 17', 'Hello, this is a test email sample 17', '<p>Html Message 17</p>', null, '2020-09-19', 0002),
(0018, 'tudorsendmail@gmail.com', 'Test Message 18', 'Hello, this is a test email sample 18', '<p>Html Message 18</p>', null, '2020-09-13', 0002),
(0019, 'tudorsendmail@gmail.com', 'Test Message 19', 'Hello, this is a test email sample 19', '<p>Html Message 19</p>', null, '2020-09-02', 0003),
(0020, 'tudorsendmail@gmail.com', 'Test Message 20', 'Hello, this is a test email sample 20', '<p>Html Message 20</p>', null, '2020-07-03', 0002);


CREATE TABLE Attachments (
    attachmentID int(4) NOT NULL auto_increment,
    attachname varchar(50),
    cid VARCHAR(50),
    attachedFile longblob,
    isEmbedded boolean,
    PRIMARY KEY(attachmentID)
);

INSERT INTO Attachments(attachmentID, attachname, cid, attachedFile, isEmbedded) values
(0001, 'dolphin', 'dolphin.jpg', null, false),
(0002, 'cat', 'cat.jpg', null, true),
(0003, 'dog', 'dog.jpg', null, false),
(0004, 'owl', 'owl.jpg', null, false);

CREATE TABLE EmailAttachments (
    emailID int(4) REFERENCES Email.emailID, 
    attachmentID int(4) REFERENCES Attachments.attachmentID
);

INSERT INTO EmailAttachments(emailID, attachmentID) values
(0001, 0001),
(0006, 0002),
(00013, 0003),
(00019, 0004);

CREATE TABLE Addresses (
    addressID int(4) NOT NULL auto_increment,
    address VARCHAR(30),
    PRIMARY KEY (addressID)
);

INSERT INTO Addresses(addressID, address) values
(0001, 'tudorsendmail@gmail.com'),
(0002, 'tudorreceivemail@gmail.com'),
(0003, 'tudorcc0@gmail.com'),
(0004, 'tudorcc2@gmail.com');

CREATE TABLE ccAddresses (
    ccID int(4) NOT NULL auto_increment,
    addressID int(4) REFERENCES Addresses.addressID,
    PRIMARY KEY (ccID)
);

INSERT INTO ccAddresses(ccID, addressID) values
(0001, 0003),
(0002, 0004),
(0003, 0004);

CREATE TABLE bccAddresses (
    bccID int(4) NOT NULL auto_increment,
    addressID int(4) REFERENCES Addresses.addressID,
    PRIMARY KEY (bccID)
);
INSERT INTO bccAddresses(bccID, addressID) values
(0001, 0003),
(0002, 0004);

CREATE TABLE toAddresses (
    toID int(4) NOT NULL auto_increment,
    addressID int(4) REFERENCES Addresses.addressID,
    PRIMARY KEY(toID)
);

INSERT INTO toAddresses(toID, addressID) values
(0001, 0002),
(0002, 0002),
(0003, 0003),
(0004, 0002),
(0005, 0002),
(0006, 0002),
(0007, 0002),
(0008, 0002),
(0009, 0002),
(0010, 0002),
(0011, 0002),
(0012, 0002),
(0013, 0002),
(0014, 0002),
(0015, 0002),
(0016, 0002),
(0017, 0002),
(0018, 0002),
(0019, 0002),
(0020, 0002);

CREATE TABLE AddressLink (
    emailID int(4) REFERENCES Email.emailID,
    ccID int(4) REFERENCES ccAddresses.ccID,
    bccID int(4) REFERENCES bccAddresses.bddID,
    toID int(4) REFERENCES toAddresses.toID
);

INSERT INTO AddressLink(emailID, ccID, bccID, toID) values
(0002, 0001, 0002, 0001),
(0001, null, 0001, 0002),
(0004, null, null, 0003),
(0003, 0003, null, 0004),
(0005, 0002, null, 0005),
(0006, null, null, 0006),
(0007, null, null, 0007),
(0008, null, null, 0008),
(0009, null, null, 0009),
(0010, null, null, 0010),
(0011, null, null, 0011),
(0012, null, null, 0012),
(0013, null, null, 0013),
(0014, null, null, 0014),
(0015, null, null, 0015),
(0016, null, null, 0016),
(0017, null, null, 0017),
(0018, null, null, 0018),
(0019, null, null, 0019),
(0020, null, null, 0020);
